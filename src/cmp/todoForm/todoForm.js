import React, { Component, useState } from 'react';
import './todoForm.css'

const TodoForm = ({submit, text, discTyping}) => {
    

    return(
        <form onSubmit={submit}>
            <input
                value={text}
                onChange={discTyping}
                className="todoInput"
                type="text"
                placeholder='перевоспитать человек имя'
            />
        </form>
    ) 
    
};


export default TodoForm;