import React from 'react';
import TodoItem from '../todoItem/todoItem';
import './todoList.css';

const TodoList = ({post, deleteItem, onCheked}) => {
    
    const elem = post.map(item => {
        const {id, ...itemProps} = item;
        return(
            <div key={item.id}>
                <TodoItem
                {...itemProps}
                deleteItem={() => deleteItem(id)}
                onCheked={(e) => onCheked(id, e)}/> 
            </div>
        )
    });
    

    return(
        <div className='todoList'>
            {elem}
        </div>
    )
};
export default TodoList;