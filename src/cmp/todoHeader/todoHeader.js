import React, { Component } from 'react';
import styled from 'styled-components';
import gerb from '../../media/gerb.png';

const Header = styled.header`
    margin: 20px 0;
    .gerb{
        width: 160px;
    }
    h1{
        margin: 20px 0;
        font-size: 35px;
    }
    span{
        font-family: Звезда;
    }
`
const TodoHeader = ({compledItem, itemCount}) => {
    
    return(
        <Header>
            <img className='gerb' src={gerb}/>
            <h1>Список отправить в санаторий</h1>
            <span>успех перевоспитать <br/> {compledItem} из {itemCount} </span>
        </Header>
    )
    
};
export default TodoHeader;