import React, { Component } from 'react';
import './todoItem.css';



const TodoItem =({disc, deleteItem, onCheked, chekedItem}) => {    

    
        let classItem = 'todoItem';
        let deleteButton = 'deleteButton'
        if(chekedItem){
            classItem += ' cheked'
        };
        return(
            <div onClick={onCheked} className={classItem}>
                <span>{disc}</span>
                <button onClick={deleteItem} className={deleteButton}>Продать <br/> почка</button>
                
            </div>
        )
    }


export default TodoItem;