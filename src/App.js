import './App.css';
import TodoHeader from './cmp/todoHeader/todoHeader';
import TodoList from './cmp/todoList/todoList';
import TodoForm from './cmp/todoForm/todoForm';
import { Component } from 'react';

class App extends Component {
  state = {
    data: [
      {disc: 'Кабачбек Ташиов', chekedItem: false, id: 1}
    ],
    value: '',
  }


  onCheked = (id, e) => {
    if(e.target === e.currentTarget){
      this.setState(({data}) => {
        const idx = data.findIndex(item => item.id === id)
        const newTodo = {...data[idx], chekedItem: !data[idx].chekedItem}
        const before = data.slice(0 ,idx);
        const after = data.slice(idx + 1);
        
        return{
          data: [...before, newTodo, ...after]
        }
      })
    }
  }


  deleteItem = (id) => {
    this.setState(({data}) => {
      const idx = data.findIndex(item => item.id === id);
      const before = data.slice(0 ,idx);
      const after = data.slice(idx + 1);
      return{
        data: [...before, ...after]
      }
    })
  }

  submit = (e) => {
    e.preventDefault();
    this.setState(({data, value}) => {
      if(value !== '' && value !== null){
        const id = Date.now();
        const newItem = {disc: value, chekedItem: false, id: id}
        return{
          data: [...data, newItem],
          value: ''
        }
      } else {
        alert('Запишите заявление!')
      }
    })
  }

  discTyping = (e) => {
      this.setState({value: e.target.value});
  }

  
  render() {
  const compledItem = this.state.data.filter((item) => {return item.chekedItem}).length;
  const itemCount = this.state.data.length;
  console.log(this.state);

    return(
      <div className='mainLayer'>
        <div className='todoLayer'>
          <TodoHeader itemCount={itemCount} compledItem={compledItem}/>
          <TodoForm discTyping={this.discTyping} submit={this.submit} text={this.state.value}/>
          <TodoList post={this.state.data} onCheked={this.onCheked} deleteItem={this.deleteItem}/>
        </div>
      </div>
    )
  }
}

export default App;





